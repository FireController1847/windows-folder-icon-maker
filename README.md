# Windows Folder Icon Maker
This simple script will use many template images happily provided by me to make those really cool looking folder icons that you see for things like the downloads folder. Make anything fit in with windows!

# How to Use
You must have Node.js installed on your computer for this to work.
1. Clone this repository to your computer
2. Open a command prompt and navigate to the `src` directory
3. Run `npm install`
   - If that fails, run `npm i windows-build-tools --global` and wait for that to finish before running `npm install` again.
   - If it continues to fail, please submit an issue.
4. Find an SVG/ICO file of your choosing that you want to make a folder icon out of
5. Make a folder called "SVG" or "ICO" in `src/resources` and put your SVG/ICO into the corresponding folder
6. Edit options.jsonc and put in your filename to the appropriate spot
7. On your command prompt, run `node create.js` and wait for it to finish
8. Your output ico file will be in the src folder, enjoy!