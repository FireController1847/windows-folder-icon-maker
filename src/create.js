const fs = require("fs");
const icoToPng = require("ico-to-png");
const Jimp = require("jimp");
const jsonc = require("jsonc-parser");
const ora = require("ora");
const pngToIco = require("to-ico");
const svgc = require("svg-to-img");
const xml = require("xml-js");
let spinner;
let options;
let type;

function getSourceImage() {
    // ICO
    const ico = `./resources/ico/${options.ico}.ico`;
    if (fs.existsSync(ico)) {
        type = 0;
        return fs.readFileSync(ico);
    }
    // SVG
    const svg = `./resources/svg/${options.svg}.svg`;
    if (fs.existsSync(svg)) {
        type = 1;
        return fs.readFileSync(svg, "utf-8");
    }
    // NULL
    return null;
}
async function getTemplateImages() {
    const files = [];
    const templateNames = ["20x_1", "20x_2", "24x_1", "24x_2", "32x_1", "32x_2", "40x_1", "40x_2", "48x_1", "48x_2", "64x_1", "64x_2", "256x_1", "256x_2"];
    for (let i = 0; i < templateNames.length; i++) {
        files.push(await Jimp.read(`./resources/template/${templateNames[i]}.png`));
    }
    return files;
}
async function parseSourceImage(source, size) {
    if (type == 0) {
        // ICO
        return await Jimp.read(await icoToPng(source, size));
    } else if (type == 1) {
        // SVG
        const svg = JSON.parse(xml.xml2json(source));
        for (let i = 0; i < svg.elements.length; i++) {
            if (svg.elements[i].name == "svg") {
                svg.elements[i].attributes.height = size.toString();
                svg.elements[i].attributes.width = size.toString();
            }
        }
        const convert = await svgc.from(xml.json2xml(svg));
        return await Jimp.read(await convert.toPng());
    }
    // NULL
    return null;
}

function fail(msg, err) {
    spinner.stop();
    if (err) console.error(err);
    spinner.fail(msg);
    process.exit(1);
}
(async () => {
    // Declare Variables
    const layers = [];
    const icoSizes = [16, 20, 24, 32, 40, 48, 64, 256];
    const imageSizes = [10, 12, 16, 20, 25, 32, 120];
    const imagePositions = [[9, 8], [11, 9], [13, 13], [18, 15], [21, 15], [29, 25], [117, 105]];
    let source;
    let templates;

    // Process Options
    spinner = ora("Loading...").start();
    try {
        const errors = [];
        options = jsonc.parse(fs.readFileSync("./options.jsonc", "utf-8"), errors);
        if (errors.length > 0 || typeof options != "object") throw new Error("Options file is an invalid object!");
    } catch (e) {
        fail("Unable to read the options file", e);
    }
    if (!options || !options.svg && !options.ico) fail("Missing SVG or ICO filename in options");

    // Fetch the Source Image
    spinner.text = "Reading Files...";
    try {
        source = getSourceImage();
    } catch (e) {
        fail("Unable to process source image", e);
    }
    if (!source) fail("Unable to find SVG or ICO file");

    // Fetch the template images
    try {
        templates = await getTemplateImages();
    } catch (e) {
        fail("Unable to fetch the template images", e);
    }

    // Make the first layer
    spinner.text = "Processing...";
    try {
        layers.push(await (await parseSourceImage(source, 16)).getBufferAsync("image/png"));
    } catch (e) {
        fail("There was an error making the first layer!", e);
    }

    // Now make the rest of the layers
    for (let i = 1; i < icoSizes.length; i++) {
        try {
            const layer = new Jimp(icoSizes[i], icoSizes[i]);
            layer.composite(templates[(i - 1) * 2], 0, 0);
            layer.composite(await parseSourceImage(source, imageSizes[i - 1]), imagePositions[i - 1][0], imagePositions[i - 1][1]);
            layer.composite(templates[(i - 1) * 2 + 1], 0, 0);
            layers.push(await layer.getBufferAsync("image/png"));
        } catch (e) {
            fail(`Error processing layer ${i}`, e);
        }
    }

    // Output
    try {
        fs.writeFileSync("./out.ico", await pngToIco(layers.reverse(), { sizes: icoSizes.reverse() }));
    } catch (e) {
        fail("Error writing to output file", e);
    }
    spinner.succeed("Completed! You can find your icon in the src folder called 'out.ico'");
})().catch(e => { fail("There was an unknown error.", e); });
process.on("unhandledRejection", e => { fail("There was an unknown error.", e); });